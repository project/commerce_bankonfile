<?php

/**
 * Menu callback: displays the add bank account page.
 *
 * If there are multiple Bank Account on File payment methods available, show a list and
 * require the user to select one. Otherwise redirect to the add form.
 */
function commerce_bank_account_onfile_add_page() {
  $account = menu_get_object('user');
  $create_implements = commerce_bank_account_onfile_payment_method_implements('create callback');
  $content = array();
  foreach ($create_implements as $method_id => $method_function) {
    $payment_method_instances = _commerce_bank_account_onfile_payment_method_instances($method_id);
    foreach ($payment_method_instances as $instance_id => $payment_method) {
      $path = 'user/' . $account->uid . '/bank-accounts/add/' . drupal_hash_base64($instance_id);
      $item = menu_get_item($path);
      if ($item['access']) {
        $content[] = $item;
      }
    }
  }

  // Bypass the user/%user/bank-accounts/add listing if only one payment method is
  // available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }

  return theme('bank_account_add_list', array('content' => $content));
}

/**
 * Displays the add bank account  list.
 *
 * @ingroup themeable
 */
function theme_bank_account_add_list($variables) {
  $output = '<dl class="commerce-bank-account-onfile-add-bank-account-list">';
  foreach ($variables['content'] as $item) {
    $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
  }
  $output .= '</dl>';

  return $output;
}

/**
 * Returns the appropriate bank account form.
 */
function commerce_bank_account_onfile_bank_account_form_page($op, $bank_account, $account) {
  $payment_method = commerce_payment_method_instance_load($bank_account->instance_id);
  if ($op == 'update') {
    // This is not likely to happen, but if the payment method doesn't implement
    // the update callback, redirect the user back to the bank account listing page and
    // inform them about the error.
    if (!isset($payment_method['bank_account_onfile']['update callback'])) {
      drupal_set_message(t('We encountered an error attempting to update your bank account data. Please try again and contact us if this error persists.'), 'error');
      drupal_goto('user/' . $bank_account->uid . '/bank-accounts');
    }
  }
  else {
    drupal_set_title(t('Add a bank account'));
    // Bank account data was initialized with the anonymous user as its owner. Set the
    // owner here to the user from the menu item, so that the form will receive
    // the complete information that is needed to save the bank account.
    $bank_account->uid = $account->uid;
  }

  if ($form_callback = commerce_bank_account_onfile_payment_method_callback($payment_method, $op . ' form callback')) {
    return drupal_get_form($form_callback, $op, $bank_account);
  }
  else {
    return drupal_get_form('commerce_bank_account_onfile_account_form', $op, $bank_account);
  }
}

/**
 * Form callback: create or edit a bank account.
 */

function commerce_bank_account_onfile_account_form($form, &$form_state, $op, $bank_account) {
  // Load the credit card helper functions from the Payment module.
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.echeck');

  // Pass along information to the validate and submit handlers.
  $form_state['bank_account'] = $bank_account;
  $form_state['op'] = $op;

  $fields = array(
    'aba_code' => '',
    'acct_num' => '',
    'type' => array(
      'checking',
      'savings',
      'business_checking',
    ),
    'bank_name' => '',
    'acct_name' => '',
  );
  $defaults = array();
  if ($op == 'update') {
    $defaults = array(
      'aba_code' => $bank_account->aba_code,
      'acct_num' => $bank_account->bank_account_number,
      'type' => $bank_account->bank_account_type,
      'bank_name' => $bank_account->bank_name,
      'acct_name' => $bank_account->bank_account_name,
    );
  }
  $form += commerce_payment_echeck_form($fields, $defaults);

  if (!empty($bank_account->commerce_bankonfile_profile)) {
    $profile = commerce_customer_profile_load($bank_account->commerce_bankonfile_profile[LANGUAGE_NONE][0]['profile_id']);
  }
  else if (module_exists('commerce_addressbook')) {
    $profile_id = commerce_addressbook_get_default_profile_id($form_state['bank_account']->uid, 'billing');
    if ($profile_id) {
      $profile = commerce_customer_profile_load($profile_id);
    }
  }
  if (!isset($profile)) {
    // Create a billing profile object and add the address form.
   $profile = commerce_customer_profile_new('billing', $form_state['bank_account']->uid);
  }
  else {
    drupal_set_message(t('Billing information has been prefilled with the data from your default billing profile.'));
  }

  $form_state['commerce_customer_profile'] = $profile;
  $form['commerce_customer_profile'] = array();
  field_attach_form('commerce_customer_profile', $profile, $form['commerce_customer_profile'], $form_state);

  $form['commerce_customer_profile']['#weight'] = -1;

  $payment_method = commerce_payment_method_load($bank_account->payment_method);
  $form['bank_account']['bank_account_instance_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use as default account for payments with %method', array('%method' => $payment_method['display_title'])),
    '#default_value' => FALSE,
  );
  // Disable the checkbox if we are adding a new card and the user doesn't have
  // any other active cards with the same instance ID. Also disable it, if we
  // are updating the current default card, so the user can't uncheck the
  // checkbox.
  $existing_bank_accounts = commerce_bank_account_onfile_load_multiple_by_uid($bank_account->uid, $bank_account->instance_id, TRUE);
  if (($op == 'create' && !$existing_bank_accounts) || ($op == 'update' && $bank_account->instance_default)) {
    $form['bank_account']['bank_account_instance_default']['#default_value'] = TRUE;
    $form['bank_account']['bank_account_instance_default']['#access'] = FALSE;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ($op == 'create') ? t('Add bank account') : t('Update bank account'),
    '#suffix' => l(t('Cancel'), 'user/' . $bank_account->uid . '/bank-accounts'),
  );
  return $form;
}

/**
 * Submit callback for commerce_bank_account_onfile_account_form().
 */
function commerce_bank_account_onfile_account_form_submit($form, &$form_state) {
  $op = $form_state['op'];
  $bank_account = $form_state['bank_account'];

  $bank_account->bank_account_type = $form_state['values']['echeck']['type'];
  $bank_account->bank_name = $form_state['values']['echeck']['bank_name'];
  $bank_account->bank_account_name = $form_state['values']['echeck']['acct_name'];

  if ($op == 'create') {
    $bank_account->bank_account_number = substr($form_state['values']['echeck']['acct_num'], -4);
    $bank_account->aba_code = $form_state['values']['echeck']['aba_code'];
  }
  $bank_account->instance_default = $form_state['values']['bank_account_instance_default'];

  // Invoke the payment method's card create/update callback.
  $payment_method = commerce_payment_method_instance_load($bank_account->instance_id);
  $callback = $payment_method['bank_account_onfile'][$op . ' callback'];
  $success = FALSE;
  if (function_exists($callback)) {
    $callback_return = $callback($form, $form_state, $payment_method, $bank_account);
    if ($callback_return) {
      if ($op == 'create') {
        $bank_account_save = $callback_return;
        $confirm_message = t('A new bank account has been added.');
      }
      else {
        $bank_account_save = $bank_account;
        $confirm_message = t('The bank account has been updated.');
      }
      commerce_bank_account_onfile_save($bank_account_save);
      drupal_set_message($confirm_message);
      $success = TRUE;
    }
  }

  if (!$success) {
    if ($op == 'create') {
      drupal_set_message(t('We encountered an error attempting to save your bank account data. Please try again and contact us if this error persists.'), 'error');
    }
    else {
      drupal_set_message(t('We encountered an error attempting to update your bank account data. Please try again and contact us if this error persists.'), 'error');
    }
  }
  $form_state['redirect'] = 'user/' . $bank_account->uid . '/bank-accounts';
}

/**
 * Builds the form for deleting bank_account_onfile data.
 */
function commerce_bank_account_onfile_delete_form($form, &$form_state, $bank_account) {
  $entity_view = entity_view('commerce_bank_account_onfile', array($bank_account), 'customer', NULL, TRUE);
  $rendered_account = drupal_render($entity_view);

  return confirm_form(
    $form,
    t('Are you sure you want to delete this bank account?'),
    'user/' . $bank_account->uid . '/bank-accounts',
    $rendered_account,
    t('Delete')
  );
}

/**
 * Form submit handler: delete stored card data.
 */
function commerce_bank_account_onfile_delete_form_submit($form, &$form_state) {
  $bank_account = $form_state['build_info']['args'][0];

  // Invoke the payment method's bank account delete callback.
  $payment_method = commerce_payment_method_instance_load($bank_account->instance_id);
  $callback = $payment_method['bank_account_onfile']['delete callback'];
  if (function_exists($callback)) {
    if (!$callback($form, $form_state, $payment_method, $bank_account)) {
      // Display a message if we failed to communicate properly with the payment
      // gateway in the Bank Account on File delete callback.
      drupal_set_message(t('We encountered an error attempting to delete your bank account data. Please try again and contact us if this error persists.'), 'error');
      $form_state['redirect'] = 'user/' . $bank_account->uid . '/cards';
      return;
    }
  }

  // Delete the bank account.
  commerce_bank_account_onfile_delete($bank_account->bank_account_id);

  drupal_set_message(t('The bank account has been deleted.'));
  $form_state['redirect'] = 'user/' . $bank_account->uid . '/bank-accounts';
}
